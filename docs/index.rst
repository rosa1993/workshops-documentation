.. workshops-documentation documentation master file, created by
   sphinx-quickstart on Mon Jul  1 14:58:45 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FH Vorarlberg Schul-Workshps
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   javascript/javascript.rst
   python/python.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
